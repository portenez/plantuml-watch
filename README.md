# plantuml-watch

[Plantuml](http://plantuml.com/) is my favorite tool
to generate diagrams from code, I created this cli tool
to automatically generate svg files from `plantuml`
as you save your work.

- [NOTES](#notes)
- [Architecture](#architecture)
- [Usage](#usage)
  - [Install](#install)
  - [Start watcher](#start-watcher)
  - [Generating diagrams](#generating-diagrams)
- [Configuration](#configuration)
  - [Options](#options)
  - [Options Defaults](#options-defaults)
  - [plantuml-watch.config.js](#plantuml-watchconfigjs)
- [Using your own plantuml server](#using-your-own-plantuml-server)
  - [Using official plantuml-server docker image](#using-official-plantuml-server-docker-image)
  - [Using Docker Desktop Kubernetes](#using-docker-desktop-kubernetes)

## NOTES

- Requires a running [plantuml server](https://github.com/plantuml/plantuml-server)
- By default will use http://www.plantuml.com/plantuml. Which means ur diagrams will traverse the internet by default.

## Architecture

![architecture diagram](./architecture.plantuml.svg)

## Usage

### Install

Locally

```bash
npm install @portenez/plantuml-watch
```

or globally

```bash
npm install -g @portenez/plantuml-watch
```

### Start watcher

From the root you want to scan for plantuml file changes:


```bash
plantuml-watch # if installed globally
npx plantuml-watch # if installed locally
```

The command will output

```
[2019-11-23 00:23:02.770 +0000] INFO  (34762 on C02XC2BQJGH5): plantuml watch started
[2019-11-23 00:23:02.774 +0000] INFO  (34762 on C02XC2BQJGH5): config loaded
    plantumlServerUrl: "http://www.plantuml.com/plantuml"
    outputPathFunction: "function"
    outputFormat: "svg"
    glob: "**/*.plantuml"
```

### Generating diagrams

Create or edit a plantuml file

```
echo "node Hello" > hello.plantuml
```


SVG will be generated

```
[2019-11-23 00:27:35.077 +0000] INFO  (35458 on C02XC2BQJGH5): processed
    event: "add"
    path: "hello.plantuml"
    outputPath: "hello.plantuml.svg"
```

By default it'll generate an svg co-located with the
original file.

```
$tree                                                                       
.
├── hello.plantuml
└── hello.plantuml.svg

0 directories, 2 files

```

## Configuration

### Options


```typescript
export type OutputFormat = 'svg' | 'png' | 'txt'

export default interface Options {
  /**
   * The base url to the plantuml server
   */
  readonly plantumlServerUrl: string

  /**
   * Target format for the output.
   * As of now only `svg` is allowed
   *
   */
  readonly outputFormat: OutputFormat
  /**
   * Defines how the path where the output rendered diagram
   * will be stored based on the original plantuml file path
   *
   * @param originalPath path of the  plantuml code file
   * @param options a subset of the config options
   *
   * @returns path where the rendered diagram should be saved
   */
  readonly outputPathFunction: (
    originalPath: string,
    options: { outputFormat: OutputFormat },
  ) => string

  /**
   * Glob pattern that points to
   * the plantuml files to watch
   */
  readonly glob: string
}
```

### Options Defaults 

```javascript
const DEFAULT_CONFIG: Config = {
  plantumlServerUrl: 'http://www.plantuml.com/plantuml',

  outputFormat: 'svg',

  outputPathFunction: (
    originalPath: string,
    { outputFormat }: { outputFormat: OutputFormat },
  ): string => `${originalPath}.${outputFormat}`,

  glob: '**/*.plantuml',
}
```

### plantuml-watch.config.js

To change any option from its defaults:

- create a `plantuml-watch.config.js` file in the root of the dir
you want to scan. 
- Export an object via `module.exports` 


See example below:

```javascript
/*
*plantuml-watch.config.js
*/

const path = require("path")

module.exports = {

  // use local plantuml server
  plantumlServerUrl: 'http://localhost:5555/uml',

  // a.plantuml -> a.svg
  outputPathFunction: (originalPath, { outputFormat }) => {
    const dirname = path.dirname(originalPath)
    const fileName = path.basename(originalPath, ".plantuml")

    return `${dirname}/${fileName}.${outputFormat}`
  },
}

```

## Using your own plantuml server

### Using official plantuml-server docker image

You can start a local plantuml server to use with
this tool by using docker:

```bash
docker run --rm -p 5555:8080 plantuml/plantuml-server:tomcat
```

This will start a plantuml server on port `5555`

Then set the `plantumlServerUrl` options property accordingly. 

For Example:

```javascript
/*
*plantuml-watch.config.js
*/

const path = require("path")

module.exports = {
  // use local plantuml server
  plantumlServerUrl: 'http://localhost:5555/uml',
}
```

### Using Docker Desktop Kubernetes

see [Plantuml-server as a k8s service in your local machine](./docs/plantuml-server-k8s-service.md)
