import * as pino from 'pino'

const logger = pino({
  level: 'info',
  prettyPrint: true,
  timestamp: pino.stdTimeFunctions.isoTime,
})

export default logger
