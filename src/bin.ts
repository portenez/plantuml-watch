#! /usr/bin/env node

import 'source-map-support/register'
import * as yargs from 'yargs'
import * as watch from './commands/watchCommand'

yargs
  .command(watch)
  .scriptName('plantuml-watch')
  .help()
  .parse()
