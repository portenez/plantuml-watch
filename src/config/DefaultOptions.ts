import Options, { OutputFormat } from './Options'

export const DEFAULT_CONFIG: Options = {
  /**
   * Live plantuml
   */
  plantumlServerUrl: 'http://www.plantuml.com/plantuml',

  /**
   * By default render the diagrams as svg
   */
  outputFormat: 'svg',

  /**
   * For input a.plantuml, save rendered diagram as a.plantuml.svg
   * co-located in the same directory
   */
  outputPathFunction: (
    originalPath: string,
    { outputFormat }: { outputFormat: OutputFormat },
  ): string => `${originalPath}.${outputFormat}`,

  /**
   * Scan recursively for all files that end in `.plantuml`
   */
  glob: '**/*.plantuml',
} as const
