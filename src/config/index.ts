import Debug from 'debug'
import * as fs from 'fs'
import * as R from 'ramda'
import * as path from 'path'
import Options from './Options'
import { DEFAULT_CONFIG } from './DefaultOptions'

const debug = Debug('@portenez:plantuml-watcher:load')

const CONFIG_FILE = 'plantuml-watch.config.js'

const requireFromCWD = R.pipe(
  R.tap(file => debug('loading user config %O', { file, path: process.cwd() })),
  (file: string) => path.resolve(process.cwd(), file),
  require,
)

export const load = (): Options => {
  if (fs.existsSync(CONFIG_FILE)) {
    return {
      ...DEFAULT_CONFIG,
      ...requireFromCWD(CONFIG_FILE),
    }
  }
  debug('using default options')
  return DEFAULT_CONFIG
}
