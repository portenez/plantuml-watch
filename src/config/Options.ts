export type OutputFormat = 'svg' | 'png' | 'txt'

export default interface Options {
  /**
   * The base url to the plantuml server
   */
  readonly plantumlServerUrl: string

  /**
   * Target format for the output.
   * As of now only `svg` is allowed
   *
   */
  readonly outputFormat: OutputFormat
  /**
   * Defines how the path where the output rendered diagram
   * will be stored based on the original plantuml file path
   *
   * @param originalPath path of the  plantuml code file
   * @param options a subset of the config options
   *
   * @returns path where the rendered diagram should be saved
   */
  readonly outputPathFunction: (
    originalPath: string,
    options: { outputFormat: OutputFormat },
  ) => string

  /**
   * Glob pattern that points to
   * the plantuml files to watch
   */
  readonly glob: string
}
