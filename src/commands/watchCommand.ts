import * as chokidar from 'chokidar'
import Debug from 'debug'
import * as Config from '../config'
import { readPlantumlFile, writePlantumlFile } from '../File'
import { request } from '../PlantumlClient'
import Options, { OutputFormat } from '../config/Options'
import logger from '../logger'

const debug = Debug('@portenez:plantuml-watch:cmd:watch')

export const command = ['watch', '$0']

export const describe = 'watch for plantuml files and changes'

export const builder = {}

const isTextOutput = (format: OutputFormat): boolean =>
  format === 'svg' || format === 'txt'

type OutputOpts = Omit<Options, 'glob'>

const generateSingle = (
  { outputFormat, outputPathFunction, plantumlServerUrl }: OutputOpts,
  event: string,
) => (path: string): void => {
  debug('processing file %s', path)
  const outputPath = outputPathFunction(path, {
    outputFormat,
  })
  debug('output path generated %s', outputPath)
  debug('outputPathFunction', outputPathFunction)

  readPlantumlFile(path)
    .then(request(outputFormat, plantumlServerUrl))
    .then(writePlantumlFile(outputPath, isTextOutput(outputFormat)))
    .then(() => logger.info({ event, path, outputPath }, 'processed'))
    .catch(({ statusCode, name, options, message }) =>
      logger.error(
        { event, path, error: { statusCode, name, options, message } },
        'Error processing change',
      ),
    )
}

export function handler(): void {
  logger.info('plantuml watch started')
  const { glob, ...outputOpts } = Config.load()
  logger.info(
    {
      ...outputOpts,
      outputPathFunction: outputOpts.outputFormat ?? 'function',
      glob,
    },
    'config loaded',
  )
  chokidar
    .watch(glob, {
      ignored: /node_modules/,
      ignoreInitial: true,
    })
    .on('add', generateSingle(outputOpts, 'add'))
    .on('change', generateSingle(outputOpts, 'change'))
}
