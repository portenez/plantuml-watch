declare module 'plantuml-encoder' {
  export function encode(input: string): string
}
