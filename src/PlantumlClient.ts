import { get } from 'request-promise-native'
import Debug from 'debug'
import * as R from 'ramda'
import { encode } from 'plantuml-encoder'
import { OutputFormat } from './config/Options'

const debug = Debug('@portenez/plantuml-watcher/PlantumlClient')
const debugUrl = (actualUrl: string): void => debug('sending url %s', actualUrl)

export const request = (outputFormat: OutputFormat, baseUrl: string) => (
  plantuml: string,
): Promise<string | Buffer> =>
  R.pipe(encode, encoded =>
    get({
      encoding: outputFormat === 'png' ? null : 'utf-8',
      url: R.tap(debugUrl, `${baseUrl}/${outputFormat}/${encoded}`),
    }),
  )(plantuml)
