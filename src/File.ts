import { promisify } from 'util'
import * as fs from 'fs'

export const writePlantumlFile = (resultPath: string, isText: boolean) => (
  contents: string | Buffer,
): Promise<void> =>
  promisify(fs.writeFile)(
    resultPath,
    contents,
    isText ? { encoding: 'utf-8' } : {},
  )

export const readPlantumlFile = (file: string): Promise<string> =>
  promisify(fs.readFile)(file, { encoding: 'utf-8' })
