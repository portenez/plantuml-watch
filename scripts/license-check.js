#! /usr/bin/env node
const pino = require('pino')
const { init, asSummary } = require('license-checker')
const { promisify } = require('util')

const onlyAllow = [
  'AFLv2.1',
  'BSD',
  'MIT',
  'ISC',
  'Apache-2.0',
  'BSD-3-Clause',
  'Unilicense',
  'CC0-1.0',
  'BSD-2-Clause',
  'Apache-2.0',
  'Apache 2.0',
  'MIT *',
  'WTFPL',
  'CC-BY-3.0',
  'Unlicense',
  'Artistic-2.0',
].join(';')

const logger = pino({
  level: 'info',
  prettyPrint: { colorize: true, translateTime: true },
})

logger.info('checking dependencies')

const _init = promisify(init)

_init({
  start: process.cwd(),
  onlyAllow,
  color: true,
  summary: true,
})
  .then(asSummary)
  .then(summary => logger.info(`\n${summary}`))
  .catch(error => logger.error(error, 'input error'))
