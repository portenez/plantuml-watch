# Plantuml-server as a k8s service in your local machine 
- [1. Create a local Kubernetes cluster](#1-create-a-local-kubernetes-cluster)
- [2. Deploy a plantuml-server `k8s service`](#2-deploy-a-plantuml-server-k8s-service)
- [3. Configure `plantuml-watch`](#3-configure-plantuml-watch)

> NOTE: 
> - I tried these instructions on a MacBook.
> - You could extend this instructions to any k8s cluster.


## 1. Create a local Kubernetes cluster

1. [Install Docker Desktop](https://www.docker.com/products/docker-desktop) 
1. Follow the [instructions to create a local Kubernetes cluster](https://docs.docker.com/desktop/kubernetes/) using [Docker Desktop](https://docs.docker.com/desktop/)

## 2. Deploy a plantuml-server `k8s service`

> NOTE: What is a [kubernetes service](https://kubernetes.io/docs/concepts/services-networking/service/)
 
1. Use the provided [plantuml-service.yml](./plantuml-service.yml) 
to create a simple local service
> To change the port update `nodePort: 30000` to a port you want 
between `[30000,32767]`
2. Make sure the service was created by running `kubectl get services`.
This is a sample output
```bash
NAME         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
kubernetes   ClusterIP   10.96.0.1       <none>        443/TCP          45m
plantuml     NodePort    10.111.39.151   <none>        8080:30000/TCP   17m
```
3. Navigate to `http://localhost:30000` in your browser. 
You should now see the planuml web-ui.

## 3. Configure `plantuml-watch`

1. Create or update your `plantuml.config.js` to point to
your new local cluster. For example:
```javascript
module.exports = {

  // use local plantuml server
  plantumlServerUrl: 'http://localhost:30000/uml',
}
```
