# [2.1.0](https://gitlab.com/portenez/plantuml-watch/compare/v2.0.1...v2.1.0) (2021-04-02)


### Bug Fixes

* **config:** support node 12x (config load failing) ([ff51a83](https://gitlab.com/portenez/plantuml-watch/commit/ff51a83c75d79c7933f4d824ee7448656198ab58))
* **npm:** add missing package ([ee33bff](https://gitlab.com/portenez/plantuml-watch/commit/ee33bff5324388d65af78adea18091537d505803))
* **npm:** do not require npm token for read ([aed2c36](https://gitlab.com/portenez/plantuml-watch/commit/aed2c36aa572591d43a93e574ce2ddf25848a5bf))
* add sourcemap support ([17eed1c](https://gitlab.com/portenez/plantuml-watch/commit/17eed1c225c3d074a8dad71db29100c8451a9322))


### Features

* **version:** add --version via yargs ([cf9c6cd](https://gitlab.com/portenez/plantuml-watch/commit/cf9c6cd961d3f1eebbb30c483d211a1f6c0d96a4))

## [2.0.1](https://gitlab.com/portenez/plantuml-watch/compare/v2.0.0...v2.0.1) (2021-02-27)


### Bug Fixes

* **tsconfig:** update target to es6 ([ad16556](https://gitlab.com/portenez/plantuml-watch/commit/ad165568a77edefe209dfb1c1fc5d4c504018f62))

# [2.0.0](https://gitlab.com/portenez/plantuml-watch/compare/v1.0.0...v2.0.0) (2019-12-18)


### Bug Fixes

* fix vulnerabilities found through `yarn audit` ([7c04e48](https://gitlab.com/portenez/plantuml-watch/commit/7c04e481c981a250a020477b624d5b9579685462))


### Features

* enable png and txt as output formats ([f9f4d4b](https://gitlab.com/portenez/plantuml-watch/commit/f9f4d4bb3f4a68f9c606f0a6a258f6892172f941))


### BREAKING CHANGES

* `outputFormat` is no longer ignored.

# 1.0.0 (2019-11-26)


### Bug Fixes

* add file beign processed to debug output ([0de549f](https://gitlab.com/portenez/plantuml-watch/commit/0de549fbe4b5d71fb244797bab641a28757b898e))
* **bin.ts:** improve error message format ([8f44750](https://gitlab.com/portenez/plantuml-watch/commit/8f44750a2ebb1e4c503078ac40542aadca3e128c))


### Features

* generate plantuml on add/change  events ([afe4129](https://gitlab.com/portenez/plantuml-watch/commit/afe412977cb97919347464fbd7e294f21d29b5fc))
